# Du pain & des roses Drupal website

## Hosting

Hosting is done on K8S (k3s distribution). Currently not directly on site but it'll be eventually.
There is no staging nor dev env, only prod currently.

## CI/CD

Simple 2 stages.
First stage is the docker image build.
Second stage is swapping the prod image with the new one.
Could be automated further with config import, cache clearing, etc

## Frontend

Done using https://www.drupal.org/project/tailwindcss
