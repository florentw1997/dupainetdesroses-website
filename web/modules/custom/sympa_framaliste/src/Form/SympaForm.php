<?php
/**
 * @file
 * Contains Drupal\sympa_framaliste\Form\MessagesForm.
 */
namespace Drupal\sympa_framaliste\Form;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

class SympaForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sympa_framaliste.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sympa_form';
  }

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var ClientInterface
   */
  protected $httpClient;


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sympa_framaliste.adminsettings');


    $form['sympa_subscribe_url'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Sympa URL (eg. https://sympa.example.org/web)'),
      '#default_value'  => $config->get('sympa_subscribe_url'),
      '#size'           => 32,
      '#maxlength'      => 64,
      '#required'       => TRUE
    );

    $form['sympa_subscribe_user'] = array(
      '#type'           => 'textfield',
      '#title'          => t('User (email)'),
      '#default_value'  => $config->get('sympa_subscribe_user'),
      '#size'           => 32,
      '#maxlength'      => 64,
      '#required'       => TRUE
    );

    $form['sympa_subscribe_password'] = array(
      '#type'           => 'password',
      '#title'          => t('Password'),
      '#default_value'  => $config->get('sympa_subscribe_password'),
      '#size'           => 32,
      '#maxlength'      => 64,
      '#required'       => TRUE
    );

    $form['sympa_subscribe_list'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Sympa list name'),
      '#default_value'  => $config->get('sympa_subscribe_list'),
      '#size'           => 32,
      '#maxlength'      => 64,
      '#required'       => TRUE
    );


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('sympa_framaliste.adminsettings');

    $this->config('sympa_framaliste.adminsettings')
      ->set('sympa_subscribe_url', $form_state->getValue('sympa_subscribe_url'))
      ->set('sympa_subscribe_list', $form_state->getValue('sympa_subscribe_list'))
      ->set('sympa_subscribe_user',$form_state->getValue('sympa_subscribe_user'))
      ->set('sympa_subscribe_password',$form_state->getValue('sympa_subscribe_password'))
      ->save();
  }
}
