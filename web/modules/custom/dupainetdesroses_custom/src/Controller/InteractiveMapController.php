<?php

namespace Drupal\dupainetdesroses_custom\Controller;

use Drupal\dupainetdesroses_custom\InteractiveMapBuilder;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DecoupledMapController.
 */
class InteractiveMapController extends ControllerBase {

  /**
   * Drupal\carmeuse_decoupled_map\DecoupledMapBuilder definition.
   *
   * @var \Drupal\dupainetdesroses_custom\InteractiveMapBuilder
   */
  protected $dpedrInteractiveMapBuilder;

  /**
   * Constructs a new DecoupledMapController object.
   *
   * @param InteractiveMapBuilder $dpedr_interactive_map_builder
   */
  public function __construct( InteractiveMapBuilder $dpedr_interactive_map_builder) {
    $this->dpedrInteractiveMapBuilder = $dpedr_interactive_map_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('dupainetdesroses_custom.builder')
    );
  }

  /**
   * Getdecoupledmap.
   *
   * @return array
   *   Return Hello string.
   */
  public function getInteractiveMapProd() {
    return $this->dpedrInteractiveMapBuilder->getInteractiveMapProd();
  }

  /**
   * Getdecoupledmap.
   *
   * @return array
   *   Return Hello string.
   */
  public function getInteractiveMapActor() {
    return $this->dpedrInteractiveMapBuilder->getInteractiveMapActor();
  }

}
