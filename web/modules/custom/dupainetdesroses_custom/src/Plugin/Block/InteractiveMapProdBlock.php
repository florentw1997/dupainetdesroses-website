<?php

namespace Drupal\dupainetdesroses_custom\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'InteractiveMapBlock' block.
 *
 * @Block(
 *  id = "interactive_map_prod_block",
 *  admin_label = @Translation("Map productor block"),
 * )
 */
class InteractiveMapProdBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\dupainetdesroses_custom\InteractiveMapBuilder definition.
   *
   * @var \Drupal\dupainetdesroses_custom\InteractiveMapBuilder
   */
  protected $dpedrInteractiveMapBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->dpedrInteractiveMapBuilder = $container->get('dupainetdesroses_custom.builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->dpedrInteractiveMapBuilder->getInteractiveMapProd();
  }

}
