<?php

namespace Drupal\dupainetdesroses_custom;

/**
 * Class InteractiveMapBuilder.
 */
class InteractiveMapBuilder {

  /**
   * Constructs a new InteractiveMapBuilder object.
   */
  public function __construct() {

  }

  public function getInteractiveMapProd() {
    $build = [
      '#theme' => 'dpedr_interactive_map_prod',
      '#attached' => [
        'library' => [
          'dupainetdesroses_custom/map_app_prod',
        ],
      ],
    ];

    return $build;
  }

  public function getInteractiveMapActor() {
    $build = [
      '#theme' => 'dpedr_interactive_map_actor',
      '#attached' => [
        'library' => [
          'dupainetdesroses_custom/map_app_actor',
        ],
      ],
    ];

    return $build;
  }

}
