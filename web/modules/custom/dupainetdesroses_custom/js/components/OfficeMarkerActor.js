import React, {useEffect, useRef} from 'react';
import {Marker, Popup} from 'react-leaflet';



const OfficeMarkerActor = ({actor: {field_actor_localisation, title, field_actors_address, field_actors_mail, nid}}) => {
  const popup = useRef(null);
  useEffect(() => {
    popup.current.leafletElement.options.leaflet.map.closePopup();
  });

  return(
    <Marker position={[field_actor_localisation.lat, field_actor_localisation.lon]}>
      <Popup ref={popup}>
        <h4><a href={nid}>{title}</a></h4>
        <p>{`${field_actors_address.address_line1} ${field_actors_address.address_line2}`}</p>
        <p>{`${field_actors_address.locality}, ${field_actors_address.postal_code}`}</p>
        <a href={"mailto:" + field_actors_mail}>{`${field_actors_mail}`}</a>
      </Popup>
    </Marker>
  )
}
export default OfficeMarkerActor;
