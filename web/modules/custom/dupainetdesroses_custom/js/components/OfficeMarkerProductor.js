import React, {useEffect, useRef} from 'react';
import {Marker, Popup} from 'react-leaflet';



const OfficeMarkerProductor = ({productor: {field_productor_localisation, title, field_productor_address, nid}}) => {
  const popup = useRef(null);
  useEffect(() => {
    popup.current.leafletElement.options.leaflet.map.closePopup();
  });

  return(
    <Marker position={[field_productor_localisation.lat, field_productor_localisation.lon]}>
      <Popup ref={popup}>
        <h4><a href={nid}>{title}</a></h4>
        <p>{`${field_productor_address.address_line1} ${field_productor_address.address_line2}`}</p>
        <p>{`${field_productor_address.locality}, ${field_productor_address.postal_code}`}</p>
      </Popup>
    </Marker>
  )
}
export default OfficeMarkerProductor;
