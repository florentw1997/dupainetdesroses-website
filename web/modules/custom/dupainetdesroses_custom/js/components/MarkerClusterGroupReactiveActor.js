import OfficeMarkerActor from './OfficeMarkerActor';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import {DivIcon} from 'leaflet';
import React, {useEffect, useRef, useState} from 'react';
import JsonApi from 'jsonapi-parse';

const createClusterCustomIcon = function(cluster) {
  const size = cluster.getChildCount() * 11;
  return new DivIcon({
    iconAnchor: [size, size],
    html: `<svg width="${size * 2}" height="${size * 2}" viewBox="0 0 ${size * 2} ${size * 2}" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle opacity="0.3" cx="${size}" cy="${size}" r="${size}" fill="#0063B4"/>
        <circle cx="${size}" cy="${size}" r="10" fill="white"/>
    </svg>`,
    className: 'marker-cluster-svg',
  });
};

const MarkerClusterGroupReactiveActor = ({bounds}) => {
  const [actor, setActor] = useState([]);
  let abortControllerRef = useRef(new AbortController());
  useEffect(() => {
    const latQuery = `filter[lat][condition][path]=field_actor_localisation.lat&filter[lat][condition][operator]=BETWEEN&filter[lat][condition][value][]=${bounds._southWest.lat.toString()}&filter[lat][condition][value][]=${bounds._northEast.lat.toString()}`;
    const lngQuery = `filter[lng][condition][path]=field_actor_localisation.lon&filter[lng][condition][operator]=BETWEEN&filter[lng][condition][value][]=${bounds._southWest.lng.toString()}&filter[lng][condition][value][]=${bounds._northEast.lng.toString()}`;
    const endpoint = `/jsonapi/node/actors?${latQuery}&${lngQuery}`;

    abortControllerRef.current.abort();
    abortControllerRef.current = new AbortController();
    fetch(endpoint, {method: 'GET', signal: abortControllerRef.current.signal}).
    then(response => response.json()).
    then(json => setActor(JsonApi.parse(json).data)).
    catch((e) => {});
    return () => abortControllerRef.current.abort();
  }, [bounds, setActor]);
  return (
    <MarkerClusterGroup spiderLegPolylineOptions={{opacity: 0}}
                        showCoverageOnHover={false}
                        iconCreateFunction={createClusterCustomIcon}>
      {actor.filter(actor => actor.field_actor_localisation !== null).map((actor, index) => <OfficeMarkerActor key={index} actor={actor}/>)}
    </MarkerClusterGroup>
  );
};

export default MarkerClusterGroupReactiveActor;
