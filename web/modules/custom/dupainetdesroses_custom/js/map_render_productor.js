import ReactDOM from "react-dom";
import React, {useEffect, useRef, useState} from "react";
import { LatLngBounds } from 'leaflet';
import {Map, TileLayer, ZoomControl,Popup, Marker} from 'react-leaflet';
import JsonApi from 'jsonapi-parse';
import MarkerClusterGroupReactiveProd from './components/MarkerClusterGroupReactiveProd';


(function (Window, jQuery, Drupal) {
  Drupal.behaviors.calculatorRenderBehavior = {
    attach: function (context, settings) {
      jQuery(context).find('#root').once('map_render').each(function () {


        ReactDOM.render(<AppMap />, document.getElementById('map'));

      });
    }
  };
})(window, jQuery, Drupal);

const AppMap = ({ defaultNid : nid }) =>{
  const map = useRef(null);
  const [productor, setProductor] = useState();
  const [bounds, setBounds] = useState();
  const [isZooming, setIsZooming] = useState(false);

  let abortControllerRef = useRef(new AbortController());

  useEffect(() => {
    const endpoint = `/jsonapi/node/productor?filter[nid]=${nid}`;

    abortControllerRef.current.abort();
    abortControllerRef.current = new AbortController();
    // Fetch info on initial office
    fetch(endpoint, {method: 'GET', signal: abortControllerRef.current.signal}).
    then(response => response.json()).
    then(json => setProductor(JsonApi.parse(json).data.shift())).
    catch((e) => {});
  }, [nid]);

  const position = [50.452749700715884, 3.9511636615623726]

  return(
    <Map zoomControl={false}
         scrollWheelZoom={false}
         ref={map}
         onZoomStart={() => setIsZooming(true)}
         minZoom={3} maxBounds={new LatLngBounds([-90, -180], [90, 180])}
         center={position}
         onMoveEnd={event => setBounds(event.target.getBounds())}
         onZoomEnd={event => {setIsZooming(false);setBounds(event.target.getBounds());}}
         zoom={8}
         whenReady={() => setBounds(map.current.leafletElement.getBounds())}
         maxZoom={21}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        noWrap={true}
        attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"/>
      {bounds && !isZooming && <MarkerClusterGroupReactiveProd bounds={bounds}/>}
      <ZoomControl position="topright"/>
    </Map>
  );
}
