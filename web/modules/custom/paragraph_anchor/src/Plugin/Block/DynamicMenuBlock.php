<?php

namespace Drupal\paragraph_anchor\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a 'Dynamic Menu' Block.
 *
 * @Block(
 *   id = "dynamic_menu_block",
 *   admin_label = @Translation("Dynamic Menu Block"),
 *   category = @Translation("Dynamic Menu"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node",
 *       label = @Translation("Entity"),
 *       required = TRUE
 *     )
 *   }
 * )
 */
class DynamicMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\node\Entity\Node $node */
    if (!($node = $this->getContextValue('node'))) {
      return [];
    }
    $anchors_link = [
      '#type' => 'html_tag',
      '#tag' => 'ul',
      '#attributes' => [
        'class' => [
          'md-max:tw-w-screen',
          'tw-flex',
          'tw-justify-evenly',
          'tw-flex-col',
          'md:tw-px-20',
          'lg:tw-flex-row',
        ],
      ],
    ];
    // Find all the parag fields on the node.
    foreach (
      DynamicMenuBlock::getAllParagraphsAnchorForEntity($node) as $anchor
    ) {
      $anchors_link[] = [
        '#type' => 'html_tag',
        '#tag' => 'li',
        '#attributes' => ['class' => ['tw-mx-4', 'md:tw-mx-10', 'menu-dpedr']],
        'link' => Link::fromTextAndUrl(
          $anchor['title'],
          Url::fromUserInput(
            '#scrollto-' . $anchor['target'],
            ['attributes' => []]
          )
        )->toRenderable(),
      ];
    }
    if (empty($anchors_link)) {
      return [];
    }
    return [
      '#theme' => 'paragraph_anchor_menu',
      '#attached' => ['library' => 'paragraph_anchor/paragraph_anchor'],
      '#children' => [
        "item_list" => $anchors_link,
      ],
    ];
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return array
   */
  private static function getAllParagraphsAnchorForEntity(
    ContentEntityInterface $entity
  ) {
    $anchors = [];
    foreach ($entity->getFieldDefinitions() as $key => $field) {
      // Check for each field of the entity if we have a paragraph field.
      if ($field->getType() == 'entity_reference_revisions'
        && $field->getSetting('target_type') == 'paragraph'
      ) {
        /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
        foreach ($entity->get($key)->referencedEntities() as $paragraph) {
          // Check if the paragraph is using the anchor behavior plugin.
          if ($paragraph->getParagraphType()->hasEnabledBehaviorPlugin(
            'anchor'
          )
          ) {
            // If it's the case, check if the values are filled.
            if (($anchor_title = $paragraph->getBehaviorSetting(
                'anchor',
                'anchor_title'
              ))
              && ($anchor = $paragraph->getBehaviorSetting('anchor', 'anchor'))
            ) {
              $anchors[] = ['title' => $anchor_title, 'target' => $anchor];
            }
            // Recursively add the nested anchors, maybe the paragraph has nested paragraphs.
            array_push(
              $anchors,
              ...
              DynamicMenuBlock::getAllParagraphsAnchorForEntity($paragraph)
            );
          }
        }
      }
    }
    return $anchors;
  }


}
