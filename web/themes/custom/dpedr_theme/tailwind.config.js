module.exports = {
  prefix: 'tw-',
  important: true,
  purge: [],
  theme: {

    screens: {
      'xl-max': {'max': '1279px'},
      // => @media (max-width: 1279px) { ... }

      'lg-max': {'max': '1023px'},
      // => @media (max-width: 1023px) { ... }

      'md-max': {'max': '767px'},
      // => @media (max-width: 767px) { ... }

      'sm-max': {'max': '639px'},
      // => @media (max-width: 639px) { ... }
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }
    },

    textColor : theme =>({
      ...theme('colors'),
      vert :'#4A6D5C',
    }),

    backgroundColors: theme => ({
      ...theme('colors'),
      'primary' : '#f5f4f1',
      'secondary': '#c38b53'
    }),

    fontFamily: {
      h1:['RamonaLight', 'sans-serif'],
      display: ['RamonaBold', 'sans-serif'],
      body: ['SpaceGrotesk', 'sans-serif'],
    },

    borderColor: {
      vert :'#4A6D5C',
    },

    fontSize: {
      xs: '0.75rem',
      sm: '0.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
      '8xl': '6rem',
    },
    extend: {
      fontFamily: {
        accent: ['Sedgwick', 'sans-serif'],
      },
      colors: {
        sable: {
          '50': '#fffefe',
          '100': '#fefefe',
          '200': '#fdfcfc',
          '300': '#fbfbf9',
          '400': '#f8f7f5',
          '500': '#f5f4f1',
          '600': '#dddcd9',
          '700': '#b8b7b5',
          '800': '#939291',
          '900': '#787876'
        },

        argile: {
          '50': '#fcf9f6',
          '100': '#f9f3ee',
          '200': '#f0e2d4',
          '300': '#e7d1ba',
          '400': '#d5ae87',
          '500': '#c38b53',
          '600': '#b07d4b',
          '700': '#92683e',
          '800': '#755332',
          '900': '#604429'
        },

        poudre: {
          '50': '#fefcfe',
          '100': '#fdfafc',
          '200': '#f9f2f8',
          '300': '#f5ebf3',
          '400': '#eedbeb',
          '500': '#e7cce2',
          '600': '#d0b8cb',
          '700': '#ad99aa',
          '800': '#8b7a88',
          '900': '#71646f'
        }
      }
    },
  },
  variants: {},
  plugins: [],
};
